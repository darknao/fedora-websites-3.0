#. extracted from content/editions/workstation/download.ko.yml
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-30 14:59-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "내려받기"
msgstr ""

#: title
msgid "페도라 웍스테이션 37 내려받기."
msgstr ""

#: description
msgid "페도라 웍스테이션을 시도하기로 결정해주어 기쁘게 생각합니다. 우리는 당신이 "
"페도라 매력에 푹 빠질 것이라고 생각합니다."
msgstr ""

#: links-%3E[0]-%3Etext
msgid "출시 기록"
msgstr ""

#, read-only
#: links-%3E[0]-%3Eurl
msgid "https://fedorapeople.org/groups/schedule/f-37/f-37-all-tasks.html"
msgstr ""

#: links-%3E[1]-%3Etext
msgid "설치 안내"
msgstr ""

#, read-only
#: links-%3E[1]-%3Eurl
msgid "https://docs.fedoraproject.org/en-US/fedora/latest/install-guide/"
msgstr ""

#: links-%3E[2]-%3Etext
msgid "커뮤니티 지원"
msgstr ""

#, read-only
#: links-%3E[2]-%3Eurl
msgctxt "links->[2]->url"
msgid "https://ask.fedoraproject.org/"
msgstr ""

#: sections-%3E[0]-%3EsectionTitle
msgid "내려받기 옵션"
msgstr ""

#: sections-%3E[0]-%3EsectionDescription
msgctxt "sections->[0]->sectionDescription"
msgid "부분설명"
msgstr ""

#: sections-%3E[0]-%3Eimages
msgctxt "sections->[0]->images"
msgid "이미지"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Etitle
msgid "윈도우 또는 맥OS에서?"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Edescription
msgid "페도라를 보다 쉽게 사용 할 수 있게 하는 페도라 미디어 작성기를 사용하여 "
"시작하세요."
msgstr ""

#, read-only
#: sections-%3E[0]-%3Econtent-%3E[0]-%3Eimage
msgid "public/assets/images/media-writer-icon.png"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Etitle
msgid "리눅스에서? 또는 ISO 파일만 원하나요?"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"이들 파일의 사용 방법이 확실한가요? [더 알아보기](https://docs.fedoraproject."
"org/en-US/quick-docs/creating-and-using-a-live-installation-image/)"
msgstr ""

#: sections-%3E[1]-%3EsectionTitle
msgid "보안, 설치, 사전 적재된 랩탑"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "우리는 보안을 중요하게 생각합니다"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Edescription
msgid "한번 이미지를 내려 받으면, 보안과 무결성 모두를 위해 확인해야 합니다. "
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "내려받기를 확인합니다"
msgstr ""

#, read-only
#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Eurl
msgid "https://getfedora.org/security/"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "하지만 기다립니다! 더 있습니다."
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"약간 다른 것이 필요한가요? [이 곳에서 다른 페도라 웍스테이션 내려받기를 "
"확인하세요](https://alt.fedoraproject.org/), 이차적인 구조와 네트워키-기반 "
"설치를 특징으로 하는 이미지."
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Etitle
msgid "페도라가 사전-탑재된 랩탑"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"우리는 당신이 완전하게-지원되는 하드웨어 구성요소를 포함하는 페도라가 사전-"
"탑재된 랩탑을 가져오도록 레노버와 같은 회사와 파트너가 되었습니다. "
msgstr ""

#, read-only
#: sections-%3E[1]-%3Econtent-%3E[2]-%3Eimage
msgid "public/assets/images/lenovo-laptop.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Etext
msgid "페도라 랩탑 살펴보기"
msgstr ""

#, read-only
#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Eurl
msgid "https://fedoramagazine.org/lenovo-fedora-now-available/"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "페도라 미디어 작성기에 대허 더 알아보기"
msgstr ""

#: sections-%3E[2]-%3EsectionDescription
msgid ""
"Getting going with Fedora is easier than ever. All you need is a 2GB USB "
"flash drive, and Fedora Media Writer.\n"
"\n"
"Once Fedora Media Writer is installed, it will set up your flash drive to "
"run a \"Live\" version of Fedora Workstation, meaning that you can boot it "
"from your flash drive and try it out right away without making any permanent "
"changes to your computer. Once you are hooked, installing it to your hard "
"drive is a matter of clicking a few buttons*."
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Edescription
msgid ""
" * 페도라는 성공적으로 설치하고 동작하려면, 최소 20GB 디스크, 2GB 램이 "
"필요합니다. 최소 자원의 두 배가 추천됩니다. "
msgstr ""

#: sections-%3E[2]-%3Eimages
msgid "public/assets/images/monitor-2.png"
msgstr ""

#: sections-%3E[3]-%3EsectionTitle
msgid "공식적으로 지원되는 페도라 커뮤니티 공간"
msgstr ""

#: sections-%3E[3]-%3EsectionDescription
msgctxt "sections->[3]->sectionDescription"
msgid "부분설명"
msgstr ""

#: sections-%3E[3]-%3Eimages
msgctxt "sections->[3]->images"
msgid "이미지"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Etitle
msgid "페도라 토의"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eimage
msgid "public/assets/images/fedora-discussion-plus-icon.png"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[0]-%3Elink-%3Eurl
msgid "https://discussion.fedoraproject.org/"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Etitle
msgid "페도라 대화"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[1]-%3Elink-%3Eurl
msgid "https://matrix.to/#/#matrix-meta-chat:fedoraproject.org"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eimage
msgid "public/assets/images/fedora-chat-plus-element.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Etitle
msgid "페도라 질문(Ask)"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[2]-%3Eimage
msgid "public/assets/images/ask-fedora.png"
msgstr ""

#, read-only
#: sections-%3E[3]-%3Econtent-%3E[2]-%3Elink-%3Eurl
msgctxt "sections->[3]->content->[2]->link->url"
msgid "https://ask.fedoraproject.org/"
msgstr ""

#: sections-%3E[4]-%3EsectionTitle
msgid "커뮤니티-관리 공간"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Etitle
msgid "레딧"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "/r/fedora"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[0]-%3Elink-%3Eurl
msgid "https://www.reddit.com/r/Fedora/"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[0]-%3Eimage
msgid "public/assets/images/reddit-brands-3.png"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Etitle
msgid "디스코드"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Elink-%3Etext
msgid "페도라 디스코드"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[1]-%3Elink-%3Eurl
msgid "https://discord.com/invite/fedora"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[1]-%3Eimage
msgid "public/assets/images/discord-brands-2.png"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Etitle
msgctxt "sections->[4]->content->[2]->title"
msgid "텔레그램"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[2]-%3Eimage
msgid "public/assets/images/telegram-brands.png"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Elink-%3Etext
msgctxt "sections->[4]->content->[2]->link->text"
msgid "텔레그램"
msgstr ""

#, read-only
#: sections-%3E[4]-%3Econtent-%3E[2]-%3Elink-%3Eurl
msgid "https://fedoraproject.org/wiki/Telegram"
msgstr ""

#: sections-%3E[5]-%3EsectionTitle
msgid "페도라를 눌러서 내려 받으면, 다음 이용 약관을 따르고 준수 할 것을 동의합니다."
msgstr ""

#: sections-%3E[5]-%3EsectionDescription
msgid ""
"페도라 소프트웨어 내려 받을 때에, 다음과 같은 모든 사항을 이해하고 있음을 "
"인정해야 합니다: 페도라 소프트웨어와 기술정보는 미국 수출 관리 규정(the "
"“EAR”)과 미국을 제외한 다른 국가와 외국 법규를 따르며, 수출 및 재-수출 또는 "
"양도 되지 않을 수 있습니다. (a) EAR의 740 부 (현재 쿠바,이란, 북한, 수단 및 "
"시리아)에 대한 부록 1의 국가 그룹 E:1에 나열된 모든 국가 (b) 금지 된 목적지 "
"또는 미국 정부; 또는 다른 국가의 연방 기관에 의해 미국 수출 거래 참여가 금지 "
"된 최종 사용자 (c) 핵, 화학 또는 생물학적인 무기, 로켓 시스템, 우주 발사체, "
"발사 로켓 또는 무인 항공기 시스템의 설계, 개발 또는 생산과 연관된 사용 목적. "
"이들 국가 중 하나에 위치하거나 이러한 제한이 적용되는 경우 페도라 소프트웨어 "
"또는 기술 정보를 내려 받을 수 없습니다. 이들 국가 중 하나 또는 이들 제한과 "
"연관되어 있는 다른 곳에 위치한 개인이나 단체에 제공하는 페도라 소프트웨어 "
"또는 기술 정보를 제공 받을 수 없습니다. 수입, 수출 및 페도라 소프트웨어와 "
"기술 정보의 사용에 대해 적용하는 경우 외국 법규 요구 조건을 만족 할만한 "
"책임이 있습니다."
msgstr ""
